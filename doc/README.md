### Data Structure 1.0

To keep consistenty entries should be kept in a consistent format please make sure they follow the template.
A template can be found in [template.md](./doc/template.md)

# License

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License][license].
[license]: http://creativecommons.org/licenses/by-sa/4.0/

