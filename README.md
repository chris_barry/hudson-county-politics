# Hudson County Politics Wiki

This wiki is intended to keep a centralized location to keep an eye on public figures in Hudson County.
This should help any curious person learn about Hudson County Politics without having to go through years of news archives.

## Reading Introduction

- [Federal](./entries/place-federal.md)
- [State](./entries/place-new-jersey.md)
- [Hudson County](./entries/place-hudson-county.md)

- [Bayonne](./entries/place-bayonne.md)
- [East Newark](./entries/place-east-newark.md)
- [Guttenberg](./entries/place-guttenberg.md)
- [Hoboken](./entries/place-hoboken.md)
- [Harrison](./entries/place-harrison.md)
- [Jersey City](./entries/place-jersey-city.md)
- [Kearny](./entries/place-kearny.md)
- [North Bergen](./entries/place-north-bergen.md)
- [Secaucus](./entries/place-secaucus.md)
- [Union City](./entries/place-union-city.md)
- [Weehawken](./entries/place-weehawken.md)
- [West New York](./entries/place-west-new-york.md)

