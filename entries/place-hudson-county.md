# Hudson County, New Jersey

## Represenatives

- County Executive: [Thomas A. DeGise](./entries/people-thomas-j-degise.md)
- Board Members: 
    - District 1: [Kenneth Kopacz](./entries/people-kenneth-kopacz.md)
    - District 2: [William O'Dea](./entries/people-william-o-dea.md)
    - District 3: [Jerry Walker](./entries/people-jerry-walker.md)
    - District 4: [Joel Torres](./entries/people-joel-torres.md)
    - District 5: [Anthony L. Romano, Jr.](./entries/people-anthony-l-romano-jr.md)
    - District 6: [Fanny J. Cedeño](./entries/people-fanny-j-cedeno.md)
    - District 7: [Caridad Rodriguez](./entries/people-caridad-rodriguez.md)
    - District 8: [Anthony P. Vainieri, Jr.](./entries/people-anthony-p-vainieri-jr.md)
    - District 9: [Albert J. Cifelli](./entries/people-albert-j-cifelli.md)

