# Donald Payne Jr.

## Facts

- Wikipedia: <https://en.wikipedia.org/wiki/Bill_Pascrell>. 
- Twitter: [@BillPascrell](https://twitter.com/BillPascrell)
- Positions Held:
    - Represenatives for the [10th District](./entries/place-federal.md) 2012 -- present
    - Former for the [8th District](./entries/place-federal.md) 1997 -- 2013
    - Former Mayor of Paterson, New Jersey 1990 -- 1997
    - Former [State Assemblyperson](./entries/-place-new-jersey.md) 1988 -- 1997 

## Events

