# Bayonne, New Jersey

## Represenatives

- Federal Disticts:
    - [Federal District 8](./entries/place-federal.md) represented by Senator [Corey Booker](./entries/people-corey-booker.md), Senator [Robert Menendez](./entries/people-robert-menendez.md), and Represenative [Albio Sires](./entries/people-albio-sires.md)
    - [Federal District 10](./entries/place-federal.md) represented by Senator [Corey Booker](./entries/people-corey-booker.md), Senator [Robert Menendez](./entries/people-robert-menendez.md), and Represenative Donald Payne Jr
- [State District 31](./entries/place-new-jersey.md) represented by Senator Sandra Bolden Cunningham, Assemblyperson Nicholas Chiaravalloti, and Assemblyperson Angela V. McKnight
- [County Freeholder](./entries/place-hudson-county.md): [Kenneth Jopacz](./entries/people-kenneth-kopacz.md)
- Mayor: James Davis
