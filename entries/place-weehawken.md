# Weehawken, New Jersey

## Represenatives

- [Federal District 8](./entries/place-federal.md) represented by Senator [Corey Booker](./entries/people-corey-booker.md), Senator [Robert Menendez](./entries/people-robert-menendez.md), and Represenative [Albio Sires](./entries/people-albio-sires.md)
- [State District 33](./entries/place-new-jersey.md) represented by Senator [Brian P. Stack](./entries/people-brian-p-stack.md), Assemblyperson Annette Chaparro, and Assemblyperson Raj Mukherji
- [County Freeholder](./entries/place-hudson-county.md): [Caridad Rodriguez](./entries/people-caridad-rodriguez.md)
- Mayor: Richard F. Turner
