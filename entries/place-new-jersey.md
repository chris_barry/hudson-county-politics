# Hudson County's State Representaion

## District 31

Covers [Bayonne](./entries/place-bayonne.md) and [Jersey City](./entries/place-jersey-city.md).

### Senate
- Sandra Bolden Cunningham 

### House
- Nicholas Chiaravalloti
- Angela V. McKnight

## District 32
Covers [East Newark](./entries/place-east-newark.md), [Guttenberg](./entries/place-guttenberg.md), [Harrison](./entries/place-harrison.md), [Kearny](./entries/place-kearney.md), [North Bergen](./entries/place-north-bergen.md), [Secaucus](./entries/place-secaucus.md), and [West New York](./entries/place-west-new-york.md).

### Senate
- [Nicholas J. Sacco](./entries/people-nicholas-j-sacco.md)

### House
- Pedro Mejia
- Angelica M. Jimenez

## District 33

Covers [Hoboken](./entries/place-hoboken.md), [Union City](./entries/place-union-city.md), [Weehawken](./entries/place-weehawken), as well as portions of [Jersey City](./entries/place-jersey-city.md).

### Senate
- [Brian P. Stack](./entries/people-brian-p-stack.md)

### House
- Annette Chaparro
- Raj Mukherji

