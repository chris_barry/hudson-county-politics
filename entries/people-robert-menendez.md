# Robert Menendez

## Facts

- Wikipedia: <https://en.wikipedia.org/wiki/Albio_Sires>
- Twitter: [@BobMenendezNJ](https://twitter.com/BobMenendezNJ)
- Positions Held:
    - Current [Federal Senator](./entries/place-federal.md) 2006 -- present
    - Former [Federal Represenative](./entries/place-federal)  1993 -- 2006
    - Former [State Senator](./entries/-place-new-jersey.md) 1991 -- 1993
    - Former [State Assemblyperson](./entries/-place-new-jersey.md) 1988 -- 1991
    - Former mayor of [Union City](./entries/place-union-city.md) 1986 -- 1992

## Events

### 2017

#### Corruption Charges Mistrial

- [Corruption Case Against Senator Menendez Ends in Mistrial](https://www.nytimes.com/2017/11/16/nyregion/senator-robert-menendez-corruption.html)

### 2015

#### Indicted on Federal Corruption Charges

- [Sen. Robert Menendez indicted on corruption charges](https://www.washingtonpost.com/politics/menendez-expected-to-be-indicted-as-soon-as-wednesday-sources-say/2015/04/01/623024c6-d86e-11e4-8103-fa84725dbf9d_story.html)
- [Senator Robert Menendez and Salomon Melgen Indicted for Conspiracy, Bribery and Honest Services Fraud](https://www.justice.gov/opa/pr/senator-robert-menendez-and-salomon-melgen-indicted-conspiracy-bribery-and-honest-services)

