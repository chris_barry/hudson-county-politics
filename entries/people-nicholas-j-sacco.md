# Nick Sacco

## Facts

- Wikipedia: <https://en.wikipedia.org/wiki/Nicholas_Sacco>
- Twitter: [@NicholasJSacco](https://twitter.com/NicholasJSacco)
- Positions Held:
    - Mayor of [North Bergen](./entries/place-north-bergen.md) 1991--present
    - New Jersey Senate for the 32nd District 1994--present

## Events

### 2020

#### Changes to Sentanceing reform

- Summary:
- Contempory news articles:
    - NBC New York: [NJ ‘Racial Justice' Sentencing Bill ‘Dead' After Similar Reductions for Corruption Added](https://www.nbcnewyork.com/news/politics/nj-racial-justice-sentencing-bill-dead-after-similar-reductions-for-corruption-added/2647062/)
- Supporting Evidence:
    - example

#### 2019

#### Voter Fraud

- Summary:
- Contempoary news articles:
    - NBC New York: [I-Team: North Bergen Public Workers May Be Committing Voter Fraud In Order to Vote In Town Elections, Sources Say](https://www.nbcnewyork.com/news/local/i-team-north-bergen-public-workers-may-be-committing-voter-fraud-in-order-to-vote-in-town-elections-sources-say/1991579/)
- Supporting Evidence:

### 2003

#### Threatening Voice Mails

- Summary: 
- Contempoary news articles:
    - nj.com: [Secaucus woman accuses state Sen. Nicholas Sacco of sexual harassment](https://www.nj.com/news/2011/12/state_sen_nicholas_sacco_accus.html)
- Supporting Evidence:
    - [Recording of the voice mails](https://www.youtube.com/watch?v=H8bYqQ5KBgE) (Warning, they are gross)
