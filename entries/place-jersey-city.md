# Jersey City, New Jersey

## Represenatives

- Federal Disticts:
    - [Federal District 8](./entries/place-federal.md) represented by Senator [Corey Booker](./entries/people-corey-booker.md), Senator [Robert Menendez](./entries/people-robert-menendez.md), and Represenative [Albio Sires](./entries/people-albio-sires.md)
    - [Federal District 10](./entries/place-federal.md) represented by Senator [Corey Booker](./entries/people-corey-booker.md), Senator [Robert Menendez](./entries/people-robert-menendez.md), and Represenative [Donald Payne Jr.](./entries/people-donald-payne-jr.md)
- State Districts:
    - [State District 31](./entries/place-new-jersey.md) represented by Senator Sandra Bolden Cunningham, Assemblyperson Nicholas Chiaravalloti, and Assemblyperson Angela V. McKnightState District: [31](./entries/place-new-jersey.md)
    - [State District 33](./entries/place-new-jersey.md) represented by Senator [Brian P. Stack](./entries/people-brian-p-stack.md), AssemblypersonAnnette Chaparro, and Assemblyperson Raj Mukherji
- [County Freeholders](./entries/place-hudson-county.md): [Kenneth Kopacz](./entries/people-kenneth-kopacz.md), [William O'Dea](./entries/people-william-o-dea.md), [Jerry Walker](./entries/people-jerry-walker.md), [Joel Torres](./people-joel-torres.md), [Anthony L. Romano, Jr.](./entries/people-anthony-l-romano-jr.md), and [Anthony P. Vainieri, Jr.](./entries/people-anthony-p-vainieri-jr.md)
- Mayor: [Steven Fulop](./entries/people-steven-fulop.md)
