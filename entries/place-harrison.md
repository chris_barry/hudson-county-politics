# Harrison, New Jersey

## Represenatives

- [Federal District 8](./entries/place-federal.md) represented by Senator [Corey Booker](./entries/people-corey-booker.md), Senator [Robert Menendez](./entries/people-robert-menendez.md), and Represenative [Albio Sires](./entries/people-albio-sires.md)
- [State District 32](./entries/place-new-jersey.md) represented by Senator [Nicholas J. Sacco](./entries/people-nicholas-j-sacco.md), Assemblyperson Pedro Mejia, and Assemblyperson Angelica M. Jimenez
- [County Freeholder](./entries/place-hudson-county.md): [Albert J. Cifelli](./entries/people-albert-j-cifelli.md)
- Mayor: James A. Fife
