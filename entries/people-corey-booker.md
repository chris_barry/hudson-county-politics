# Corey Booker

## Facts

- Wikipedia: <https://en.wikipedia.org/wiki/Cory_Booker>
- Twitter: [@senBooker](https://twitter.com/senBooker)
- Positions Held:
    - Current [Federal Senator](./entries/place-federal.md) 2013 -- present
    - Former mayor of Newark, New Jersey 2006 -- 2013

## Events


