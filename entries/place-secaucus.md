# Secaucus, New Jersey

## Represenatives

- [Federal District 9](./entries/place-federal.md) represented by Senator [Corey Booker](./entries/people-corey-booker.md), Senator [Robert Menendez](./entries/people-robert-menendez.md), and Represenative [Bill Pascrell](./entries/people-bill-pascrell.md)
- [State District 32](./entries/place-new-jersey.md) represented by Senator [Nicholas J. Sacco](./entries/people-nicholas-j-sacco.md), Assemblyperson Pedro Mejia, and Assemblyperson Angelica M. Jimenez
- [County Freeholders](./entries/place-hudson-county.md): [Caridad Rodriguez](./entries/people-caridad-rodriguez.md), [Anthony P. Vainieri, Jr.](./entries/people-anthony-p-vainieri-jr.md), [Albert J. Cifelli](./entries/people-albert-j-cifelli.md)
- Mayor: Michael Gonnelli
