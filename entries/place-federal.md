# Hudson County's Federal Representaion

## Senate

- [Corey Booker](./entries/people-corey-booker.md)
- [Robert Menendez](./entries/people-robert-menendez.md)

## House

### District 8

- [Albio Sires](./entries/people-albio-sires.md)

### District 9

- [Bill Pascrell](./entries/people-bill-pascrell.md)

### District 10

- [Donald Payne Jr.](./entries/people-donald-payne-jr.md)

