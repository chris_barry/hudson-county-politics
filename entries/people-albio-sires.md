# Albio Sires

## Facts

- Wikipedia: <https://en.wikipedia.org/wiki/Albio_Sires>
- Twitter: [@RepSires](https://twitter.com/RepSires)
- Positions Held:
    - Represenatives for the [8th District](./entries/place-federal.md) 2006 -- present
    - Former mayor of [West New York](./entries/place-west-new-york.md) 1995 -- 2006

## Events

### 2019

#### Relationship with Elliot Abrams

- Summary: 
- Contempoary news articles:
- Supporting Evidence:
    - [Facebook post from Represenative Sires](https://www.facebook.com/RepAlbioSires/photos/a.10150318077828751/10156547721133751/?type=3&theater)

### 1997

#### Corruption in [West New York](./entries/place-west-new-york.md) Police Department

- Summary: 
- Contempoary news articles:
    - [Police Chief in West New York Corruption Inquiry Resigns](https://www.nytimes.com/1997/10/01/nyregion/police-chief-in-west-new-york-corruption-inquiry-resigns.html)

### 1986

#### Ran for House of Represenatives as Republican

- Summary: 
- Contempoary news articles:
    - NYTimes: [POLITICS; 3 HOUSE RACES GET NATIONAL ATTENTION](https://www.nytimes.com/1986/09/21/nyregion/politics-3-house-races-get-national-attention.html)
- Supporting Evidence:
    - [Race details](https://www.ourcampaigns.com/RaceDetail.html?RaceID=30747)
